const path = require('path')
const http = require('http')
const express = require('express')
const WebSocket = require('ws')
const Redis = require('ioredis')
const redis = new Redis(process.env.REDIS_HOST || 'redis')
const morgan = require('morgan')

const app = express()
const server = http.createServer(app)
const wss = new WebSocket.Server({ server })

app.use(morgan('combined'))
app.use(express.static(path.join(__dirname, 'public')))

wss.broadcast = function broadcast (data) {
  wss.clients.forEach(client => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(JSON.stringify(data))
    }
  })
}

let localTotal
wss.on('listening', async () => {
  localTotal = parseInt(await redis.get('total')) || 0

  setInterval(async () => {
    let current = parseInt(await redis.get('total')) || 0
    if (current !== localTotal) await redis.set('total', localTotal)

    wss.broadcast({ total: localTotal })
  }, 1000)
})

wss.on('connection', ws => {
  ws.on('message', raw => {
    try {
      let message = JSON.parse(raw)
      let count = parseInt(message.count) || 0
      if (count > 100) count = 100

      localTotal += count
    } catch (err) {
      // Do nothing lol
    }
  })
})

setInterval(async () => {
  await redis.bgrewriteaof()
}, 1000 * 60)

server.listen(3000)
