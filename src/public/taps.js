let toSend = 0
let ws = new WebSocket(`${location.protocol === 'https:' ? 'wss' : 'ws'}://${location.host}`)
let countElem = new CountUp('global', 0, 0)
countElem.start()

const tap = () => { // eslint-disable-line
  let local = parseInt(localStorage.getItem('count')) || 0

  local++
  toSend++

  document.getElementById('local').innerText = Humanize.intComma(local)
  localStorage.setItem('count', local)
}

const onLoad = () => {
  let local = parseInt(localStorage.getItem('count')) || 0
  document.getElementById('local').innerText = Humanize.intComma(local)
}
onLoad()

setInterval(() => {
  ws.send(JSON.stringify({ count: toSend }))
  toSend = 0
}, 1000)

ws.onmessage = e => {
  let { total } = JSON.parse(e.data)
  countElem.update(total)
}
